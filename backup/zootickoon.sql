-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : Dim 29 mai 2022 à 23:11
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `zootickoon`
--

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `place` varchar(64) NOT NULL,
  `start` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `duration` int(11) NOT NULL,
  `image` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `events`
--

INSERT INTO `events` (`id`, `name`, `description`, `place`, `start`, `duration`, `image`) VALUES
(1, 'Spectacle de rapaces', 'Venez admirez les differents rapaces venus de partout dans le monde pour vous émerveiller au cours d\'un tas de différentes animations et épreuves.', 'Volière Ouest', '2022-06-01 14:00:00', 60, '968410.jpg'),
(2, 'Spectacle des orques', 'Les dresseurs vous proposent un impressionnant show avec nos trois orques', 'Bassin des orques', '2022-06-02 09:00:00', 120, '987409.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(11) NOT NULL,
  `datet` datetime NOT NULL DEFAULT current_timestamp(),
  `login` int(11) NOT NULL,
  `sujet` varchar(50) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `prio` tinyint(4) NOT NULL DEFAULT 0,
  `secteur` varchar(50) NOT NULL,
  `statut` enum('EN COURS','RESOLU','ANNULE','') NOT NULL DEFAULT 'EN COURS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`id`, `datet`, `login`, `sujet`, `description`, `prio`, `secteur`, `statut`) VALUES
(1, '2020-07-30 14:37:49', 0, 'fuite d\'eau', 'fuite d\'eau dans le local des lions', 0, 'savanne', 'EN COURS'),
(3, '2022-05-29 20:11:56', 8, 'zzzz', 'zefzef', 2, 'azd', 'EN COURS'),
(4, '2022-05-29 20:34:27', 8, 'zeargerzgergerzgergzergaefgr', 'aqerhgezrge&#039;zqsrgersge cezrtg&#039;terygvcert-ychretgefkjhzvbfoiuzhrbfoizerjfbnzpeioufjbzepiofuaqerhgezrge&#039;zqsrgersge cezrtg&#039;terygvcert-ychretgefkjhzvbfoiuzhrbfoizerjfbnzpeioufjbzepiofuaqerhgezrge&#039;zqsrgersge cezrtg&#039;terygvcert-ychretgefkjhzvbfoiuzhrbfoizerjfbnzpeioufjbzepiofu', 2, 'aergaezrga', 'EN COURS');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `mail` varchar(1024) NOT NULL,
  `hpass` varchar(1024) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `mail`, `hpass`, `admin`) VALUES
(1, 'NiwdEE', 'ed.alb180@gmail.com', '47022f2523d1b42d811bb76f4ecd55f337efd4a2aaca82e730a58f7f8fca114a', 0),
(8, 'admin', 'admin@CergyZoo.fr', '6d00d214b913f7ef77b2fd78740919b1ba330747c1e484e00565bae77f084dd9', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
