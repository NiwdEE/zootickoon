<?php

//Be sure relative paths won't cause problems
$root = realpath($_SERVER["DOCUMENT_ROOT"]);

//Get possible events list from JSON
$events = json_decode(file_get_contents("$root/assets/events/events.json"), true);
$eventsCount = sizeof($events);


$ret = []; //Array to return

//If the query needs a fake hour
if(isset($_REQUEST['hoursim'])){
    $h_m = explode('h', $_REQUEST['hoursim']);
    $msmn = intval($h_m[0]) * 60 + intval($h_m[1]);
}else{
    $msmn = intval(date('H')) * 60 + intval(date('i'));
}


//Function to compare events in time with the format $event['startTime']=[$h, $m] with $h = hour of start and $m are minutes. (Used for sorting)
function cmpEvts($e1, $e2){
    if($e1['startTime'][0] > $e2['startTime'][0]){
        return 1;
    }else if($e1['startTime'][0] < $e2['startTime'][0]){
        return -1;
    }else{
        if($e1['startTime'][1] > $e2['startTime'][1]){
            return 1;
        }else if($e1['startTime'][1] < $e2['startTime'][1]){
            return -1;
        }else{
            return 0;
        }
    }
}

foreach($events as $evt){
    //Start time of event in minutes since midnight
    $evtmsmn = $evt['startTime'][0]*60 + $evt['startTime'][1];
    
    /*
        Coming field:

            -1  Already passed
            0   Actually running
            1   Coming soon (<1h)
            2   Coming (>1h)
    */

    if($evtmsmn > $msmn){
        if($evtmsmn < $msmn + 60){ //Coming in less than an hour
            $evt['coming'] = 1;
            array_push($ret, $evt);
        }else{ //More than an hour
            $evt['coming'] = 2;
            array_push($ret, $evt);
        }
    }else if($evtmsmn+$evt['duration'] > $msmn){ //Started and still running
        $evt['coming'] = 0;
        array_push($ret, $evt);
    }
}

usort($ret, "cmpEvts"); //Sort by time

exit(json_encode($ret));
