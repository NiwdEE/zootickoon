<?php

require_once('confidential.php');
require_once('db_connect.php');
require_once('auth.php');

/*
    Exit code:

    2  Password required
    1  Mail Required
    0  OK
    -1 Invalid identifiers

*/

$db = connectToDB();

//Function to exit code and redirect to origin with error code
function endwith($code){
    if(!isset($_REQUEST["goto"])){
        exit;
    }
    $goto = $_REQUEST['goto'];
    $c = preg_match("/\?/", $goto) ? "&" : "?";
    header("Location:/$goto".$c."errorCode=$code");
    exit;
}


$mail = isset($_POST['mail']) ? $db->real_escape_string($_POST['mail']) : NULL;

//If mail is provided and corresponds to a mail address format we continue
if(is_null($mail) || !preg_match('/^[\w\-\.]+@([\w-]+\.)+[\w-]{2,4}$/', $mail)){
    endwith("1");
}


$pass = isset($_POST['pass']) ? $db->real_escape_string($_POST['pass']) : NULL;

if(is_null($pass) || $pass ==""){
    endwith("2"); 
}

$hpass = hash('SHA256', $mail.$pass.AUTH_SALT);

//Check if creditentials are correct
$qr_register = Query::newQueryDB($db, "SELECT * FROM `users` WHERE `mail`='$mail' AND `hpass`='$hpass'");

if($qr_register->rows() != 1){
    endwith("-1");
}

$res = $qr_register->data()[0];

//Fill in session infos
Auth::setSession([
    "valid" => true,
    "name" => $res['name'],
    "id" => $res['id'],
    "exp" => time() + (12*3600),
    "admin" => $res['admin']
]);

endwith("0");

?>