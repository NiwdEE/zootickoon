<?php

require_once('confidential.php');
require_once('db_connect.php');
require_once('auth.php');

/*
    Exit codes:

    4  Passwords don't match
    3  Name required
    2  Password must be at least 6 char long
    1  Invalid mail
    
    0  OK

    -1 Name already in use
    -2 Mail already in use
    -3 Unknown error :/
    
*/

function endwith($code){
    if(!isset($_REQUEST["goto"])){
        exit;
    }
    $goto = $_REQUEST['goto'];
    $c = preg_match("/\?/", $goto) ? "&" : "?";
    header("Location:/$goto".$c."errorCode=$code");
    exit;
}


$db = connectToDB();

$mail = isset($_POST['mail']) ? $db->real_escape_string($_POST['mail']) : NULL;

//If mail is provided and corresponds to a mail address format we continue
if(is_null($mail) || !preg_match('/^[\w\-\.]+@([\w-]+\.)+[\w-]{2,4}$/', $mail)){
    endwith("1");
}

//Checking all inputs

$pass = isset($_POST['pass']) ? $db->real_escape_string($_POST['pass']) : NULL;

if(is_null($pass) || strlen($pass) < 6){
    endwith("2"); 
}

$name = isset($_POST['name']) ? htmlspecialchars($_POST['name'], ENT_QUOTES, 'UTF-8') : NULL;

if(is_null($name) || strlen($name) < 1){
    endwith("3"); 
}

if(!isset($_POST['re_pass']) || $_POST['re_pass']!=$pass){
    endwith("4"); 
}

//Check if name isn't already used
$qr_check_name = Query::newQueryDB($db, "SELECT `id` FROM `users` WHERE `name`='$name'");
if($qr_check_name->rows()!=0){
    endwith("-1");
}


//Check if mail isn't already used
$qr_check_mail = Query::newQueryDB($db, "SELECT `id` FROM `users` WHERE `mail`='$mail'");
if($qr_check_mail->rows()!=0){
    endwith("-2");
}

//Hashing password with name and a salt
$hpass = hash('SHA256', $mail.$pass.AUTH_SALT);

//Register user's infos
$qr_register = Query::newQueryDB($db, "INSERT INTO `users`(`name`, `mail`, `hpass`) VALUES ('$name', '$mail', '$hpass')");

if($qr_register->error())
    endwith("-3");

//Connect user to his session
Auth::setSession([
    "valid" => true,
    "name" => $name,
    "id" => $qr_register->id(),
    "exp" => time() + (12*3600),
    "admin" => false
]);

endwith("0");

?>