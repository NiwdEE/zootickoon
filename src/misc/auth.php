<?php

class Auth{

    public $connected = false;
    public $admin = false;

    function __construct(){
        session_start();
    
        if(!isset($_SESSION['valid']) || !$_SESSION['valid']){
            $this->logout();
            return;
        }

    
        if(isset($_SESSION['exp']) && $_SESSION['exp'] > time()){ //If session is expired
            $this->connected = true;
        }else{
            $this->logout();
            return;
        }

        $this->admin = (isset($_SESSION['admin']) && $_SESSION['admin']);
    }

    function logout(){
        $_SESSION = ["valid" => FALSE];
    }

    //Fast way to set fiels of $_SESSION without erase others
    static public function setSession($params){
        foreach($params as $k=>$v){
            $_SESSION[$k] = $v;
        };
    }

}

$Auth = new Auth();


?>