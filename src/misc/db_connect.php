<?php

require_once 'confidential.php';


// Connect with the database.
function connectToDB()
{
  $connect = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  if (mysqli_connect_errno($connect)) {
    die("Failed to connect:" . mysqli_connect_error());
  }
  mysqli_set_charset($connect, "utf8");

  return $connect;
}

class Query{
    
    private $_data; //Data received
    
    private $_rowamt; //Rows amount
    private $_error = false;
    
    private $_last_id = null; //ID of the line sent
    
    function __construct($db, $query){

        $result = $db->query($query);

        if($result === false){ //If query failed
            $this->_error = true;
        }else if($result === true){ //For queries like INSERT, UPDATE, etc.
            $this->_rowamt = 0;
            $this->_last_id = $db->insert_id;
        }else{ //For query's results
        
            $this->_rowamt = 0;
            $this->_data = [];

            while($row = $result->fetch_assoc()){
                foreach($row as $key => $value){ //Mapping data into an associative array
                    $this->_data[$this->_rowamt][$key] = $value;
                }
                $this->_rowamt++;
            }
        }
        
        $this->_error = !$result;
    }

    //Query without existing database
    public static function newQuery($query){
        $qr = new Query(connectToDB(), $query);
        return $qr;
    }

    //Query with existing database
    public static function newQueryDB($db, $query){
        $qr = new Query($db, $query);
        return $qr;
    }
    

    //Getters
    public function data(){ return $this->_data; }
    
    public function rows(){ return $this->_rowamt; }
    
    public function error(){ return $this->_error; }
    
    public function errmsg() { return $db->error; }
    
    public function id() { return $this->_last_id; }

}



?>