<?php

require_once('confidential.php');
require_once('db_connect.php');
require_once('auth.php');

/*
    Exit codes:

    4  Location required
    3  Priority required
    2  Description required
    1  Subject required
    
    0  OK

    -1 Unknown Error
    -2 Must be logged in

*/

function endwith($code){
    if(!isset($_REQUEST["goto"])){
        exit;
    }
    $goto = $_REQUEST['goto'];
    $c = preg_match("/\?/", $goto) ? "&" : "?";
    header("Location:/$goto".$c."ticketCode=$code");
    exit;
    // var_dump($_POST);
}


$db = connectToDB();

//User must be connected to post a ticket
if(!$Auth->connected){
    endwith("-2");
}

//Checking inputs

$subject = isset($_POST['subject']) ? htmlspecialchars($_POST['subject'], ENT_QUOTES, 'UTF-8') : NULL;

if(is_null($subject) || strlen($subject) == 0){
    endwith("1");
}

$desc = isset($_POST['description']) ? htmlspecialchars($_POST['description'], ENT_QUOTES, 'UTF-8') : NULL;

if(is_null($desc) || strlen($desc) == 0){
    endwith("2");
}

$prio = isset($_POST['prio']) ? intval($_POST['prio']) : NULL;

if(is_null($prio) || $prio == "0"){
    endwith("3");
}


$loc = isset($_POST['location']) ? htmlspecialchars($_POST['location'], ENT_QUOTES, 'UTF-8') : NULL;

if(is_null($loc) || strlen($loc) == 0){
    endwith("4");
}

$uid = $_SESSION['id'];

//Save ticket
$qr_save_ticket = Query::newQuery("INSERT INTO `ticket`(`login`, `sujet`, `description`, `prio`, `secteur`) VALUES ('$uid', '$subject', '$desc', '$prio', '$loc')");

if($qr_save_ticket->error()){
    endwith("-1");
}


endwith("0");

?>