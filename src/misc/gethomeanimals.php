<?php

//Be sure relative paths won't cause problems
$root = realpath($_SERVER["DOCUMENT_ROOT"]);

//Get possible animals list from JSON
$animals = json_decode(file_get_contents("$root/assets/home/home.json"), true);
$animalsCount = sizeof($animals);

$qtt = 3; //Quantity of animals requested

if(isset($_REQUEST['quantity']) && ($_qtt=intval($_REQUEST['quantity']))){
    $qtt = min($_qtt, $animalsCount);
}

$ret = []; //Array to return

for($i=0; $i < $qtt; $i++){
    //Index of the element to send
    $index = rand(0, $animalsCount - $i - 1);

    array_push($ret, $animals[$index]); //Add it to response

    array_splice($animals, $index, 1); //Splice it to not send it twice
}

exit(json_encode($ret)); //Send result a JSON

?>