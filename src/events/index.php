<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="/styles.css"></link>
    <link rel="stylesheet" href="events.css"></link>
    <link rel="stylesheet" href="/materials/navbar.css"></link>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css"></link>


    <?php //Imports
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/materials/navbar.php");
    require_once("$root/misc/db_connect.php");
    ?>

    <?php
        $title="CergyZoo";

        foreach($Navbar->Tabs as $t){
            if($t['path'] == $_SERVER['REQUEST_URI']){
                $title = $t['name'] . ' - CergyZoo'; 
            }
        }
    ?>
    <title><?=$title?></title>

        
</head>

<body>

    <?=$Navbar->HTML?>

    <?php

    //Get all events, most recent first
    $qr = Query::newQuery("SELECT * FROM events WHERE start > NOW() ORDER BY start");
    $events = $qr->data();

    // var_dump($events);
    ?>

    <div id="title">
        Activité à venir
    </div>

    <div id="events">


        <?php foreach($events as $i=>$evt): ?>

        <div class="event n<?=$i?>" onClick="expand(<?=$i?>)">
            <div class="image">
                <img src="/assets/images/<?=$evt['image']?>" />
            </div>
            <div class="infos">
                <div class="top">
                    <div class="name">
                        <?=$evt['name']?>
                    </div>
                    <div class="start">
                        <?=$evt['start']?>
                    </div>
                </div>
                <div class="desc">
                    <?=$evt['description']?>
                </div>
            </div>
            
            
        </div>

        <?php endforeach; ?>
    </div>

    
    <?=$Navbar->script?>

    <script>
        function expand(n){
            let evt = document.querySelector('div.event.n'+n);
            if(!evt) return;
            evt.classList.toggle("expanded");
        }
    </script>

</body>


</html>