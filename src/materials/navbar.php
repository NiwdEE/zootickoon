<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/misc/auth.php");

$Navbar = new stdClass();

$Navbar->Tabs = [
    [
        "name" => "Accueil",
        "path" => "/"
    ],
    [
        "name" => "Activités",
        "path" => "/events/"
    ],
    [
        "name" => "Un problème ?",
        "path" => "/formTicket/"
    ]
];

if($Auth->connected){
    $goto = $_SERVER['REQUEST_URI'];
    array_push($Navbar->Tabs,[
        "name" => "Déconnexion",
        "path" => "/misc/logout.php?goto=$goto"
    ]);
}

if($Auth->admin){
    array_push($Navbar->Tabs,[
        "name" => "Tickets",
        "path" => "/myTickets?admin"
    ]);
}


function isactive($path){
    $rpath = explode("?", $_SERVER['REQUEST_URI'])[0];

    return $path==$rpath;
}


//Storing Navbar HTML code and Script to reuse them easier

ob_start(); ?>

<div id="navbar">
    <?php foreach($Navbar->Tabs as $tab):?>
        <div class="navbar-tab noselect <?= isactive($tab['path'])?'active':'' ?>" onClick="goto('<?= $tab['path'];?>')">
            <span class="navbar-tab-name">
                <?= $tab['name']; ?>
            </span>
        </div>
    <?php endforeach;?>
</div>

<?php $Navbar->HTML = ob_get_clean(); ?>



<?php ob_start();?>

<script>
    function goto(path){
        window.location.href = path;
    }
</script>

<?php $Navbar->script = ob_get_clean(); ?>