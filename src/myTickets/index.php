<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="/styles.css"></link>
    <link rel="stylesheet" href="myTickets.css"></link>
    <link rel="stylesheet" href="/materials/navbar.css"></link>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css"></link>

    <link rel="icon" type="image/x-icon" href="/assets/logo.jpg" />


    <?php //Imports
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/materials/navbar.php");
    require_once("$root/misc/db_connect.php");
    require_once("$root/misc/auth.php");
    ?>

    <?php
        $title="CergyZoo";

        foreach($Navbar->Tabs as $t){
            if($t['path'] == $_SERVER['REQUEST_URI']){
                $title = $t['name'] . ' - CergyZoo'; 
            }
        }
    ?>
    <title><?=$title?></title>

        
</head>

<body>

    <?=$Navbar->HTML?>

    <?php

    if(!isset($_SESSION['id'])){
        header("Location:/formTicket");
    }
    $id = $_SESSION['id'];
    $showadmin = false;

    if(isset($_REQUEST['admin']) && $Auth->admin){
        $showadmin = true;
        $qr_tickets = Query::newQuery("SELECT ticket.*, users.name FROM ticket LEFT JOIN users ON ticket.login=users.id");
    }else{
        $qr_tickets = Query::newQuery("SELECT * FROM `ticket` WHERE `login`=$id");
    }


    $tickets = $qr_tickets->data();

    $prioNames = [
        "0"=> "Non définie",
        "1"=> "Très faible",
        "2"=> "Faible",
        "3"=> "Normale",
        "4"=> "Élevée",
        "5"=> "Très élevée",
        "6"=> "Critique"
    ]

    ?>

    <div id="main-content">
        <h1 id="title">
            Mes tickets
        </h1>
        <div id="tickets">

            <?php if(!$qr_tickets->rows()): ?>
            
            <div id="nothing-here">
                Vous n'avez aucun ticket à votre actif...
            </div>

            <?php else: foreach($tickets as $ticket): ?>

            <div class="ticket">
                <div class="ticket-prop subject">
                    <span class="ticket-prop-name">Sujet:</span>
                    <span><?=$ticket['sujet']?></span>
                </div>
                <div class="ticket-prop desc">
                    <span class="ticket-prop-name">Description:</span>
                    <div class="description-content">
                        <span><?=$ticket['description']?></span>
                    </div>
                </div>
                <div class="ticket-prop location">
                    <span class="ticket-prop-name">Secteur:</span>
                    <span><?=$ticket['secteur']?></span>
                </div>
                <div class="ticket-prop prio">
                    <span class="ticket-prop-name">Priorité:</span>
                    <span>
                        <?=$prioNames[$ticket['prio']]?>
                    </span>
                </div>
                <div class="ticket-prop date">
                    <span class="ticket-prop-name">Posté le:</span>
                    <span><?=$ticket['datet']?></span>
                    <?php if($showadmin): //Show user that posted the ticket in admin mode?>
                        <span class="ticket-prop-name">par:</span>
                        <span><?=$ticket['name']?></span>
                    <?php endif; ?>
                </div>
                <div class="ticket-prop status">
                    <span class="ticket-prop-name">Statut:</span>
                    <span><?=$ticket['statut']?></span>
                </div>
                
                
                
            </div>

            <?php endforeach; endif; ?>

            

        </div>
    </div>


    <?=$Navbar->script?>

</body>


</html>