<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="styles.css"></link>
    <link rel="stylesheet" href="indexstyle.css"></link>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css"></link>
    <link rel="stylesheet" href="materials/navbar.css"></link>

    <?php //Imports
    require_once('materials/navbar.php');
    require_once('misc/db_connect.php');
    ?>

    <?php
        $title="CergyZoo";

        foreach($Navbar->Tabs as $t){
            if($t['path'] == $_SERVER['REQUEST_URI']){
                $title = $t['name'] . ' - CergyZoo'; 
            }
        }
    ?>
    <title><?=$title?></title>

        
</head>



<body>
    <?=$Navbar->HTML?>
    
    <div id="main-content">
        <h1 id="main-title">
            CergyZoo
        </h1>

        <div id="animals-block">
            <h1 id="animals-title">
                Et pourquoi pas venir voir :
            </h1>
            <div id="animals" class="tile-container"></div>
        </div>

        <div id="events-block">
            <h1 id="events-title">
                Évenement(s) :
            </h1>
            <div id="events" class="tile-container">
                <span id="nothing-here">Aucun événement en cours ou imminent</span>
            </div>
        </div>
        
    </div>


    <?=$Navbar->script?>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script>

    var animals = [];
    var animals = [];

    function normalizeTime(arr){
        arr.forEach((v, i)=>{
            arr[i] = "" + (v<10?"0":"") + v;
        })
    }

    function refreshAnimals(){
        // console.log(animals);
        
        $("#animals").empty();

        animals.forEach((animal)=>{
            $("#animals").append(`
            <div class="animal tile">
                <img src="/assets/home/${animal.img}">
                <div class="title">
                    ${animal.title}
                </div>
            </div>
            `);
        })

    }

    

    function getAnimals(){
        $.ajax("http://localhost/misc/gethomeanimals.php",{
            type: "GET",
            data: {"quantity": 3},
            success: function(res) {
                animals = JSON.parse(res);
                refreshAnimals();
            }
        });
    }


    function refreshEvents(){
        // console.log(events);
        
        if(!events.length) return;

        $("#events").empty();

        events.forEach((event)=>{
            
            let status = "";
            if(event.coming == 1){
                status = `
                <div class="status incoming">
                    Imminent
                </div>
                `
            }else if(event.coming == 0){
                status = `
                <div class="status running">
                    En cours
                </div>
                `
            }

            let s_h = event.startTime[0];
            let s_m = event.startTime[1];

            let e_h = parseInt(event.duration/60) + s_h;
            let e_m = (event.duration + s_m)%60;
            

            let t=[s_h,s_m, e_h, e_m]

            // console.log(t);

            normalizeTime(t);

            let time = `${t[0]}:${t[1]}-${t[2]}:${t[3]}`
            
            $("#events").append(`
            <div class="event tile">
                <img src="/assets/events/${event.img}">
                <div class="title">
                    <span>${event.title}</span>
                    <span>${time}</span>
                </div>
                ${status}
            </div>
            `);
        })

    }

    function getEvents(){
        $.ajax("http://localhost/misc/gethomeevents.php",{
            type: "GET",
            <?php if(isset($_REQUEST['hoursim'])): ?>
                data: {"hoursim": "<?=$_REQUEST['hoursim']?>"},
            <?php endif; ?>
            success: function(res) {
                // console.log(res)
                events = JSON.parse(res);

                refreshEvents();
            }
        });
    }
    

    $(document).ready(()=>{
        getAnimals();
        getEvents();
        
        setInterval(()=>{
            getAnimals();
        }, 10000)

        setInterval(() => {
            getEvents();
        }, 600000);
    });


    </script>

</body>


</html>