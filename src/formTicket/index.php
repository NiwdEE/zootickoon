<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="icon" type="image/x-icon" href="/assets/logo.jpg" />
    <link rel="stylesheet" href="/styles.css"></link>
    <link rel="stylesheet" href="formTicket.css"></link>
    <link rel="stylesheet" href="/materials/navbar.css"></link>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.css"></link>


    <?php //Imports
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/materials/navbar.php");
    require_once("$root/misc/db_connect.php");
    require_once("$root/misc/auth.php");
    ?>

    <?php
        $title="CergyZoo";

        foreach($Navbar->Tabs as $t){
            if($t['path'] == $_SERVER['REQUEST_URI']){
                $title = $t['name'] . ' - CergyZoo'; 
            }
        }
    ?>
    <title><?=$title?></title>

        
</head>

<body>

    <?=$Navbar->HTML?>

    <?php

    $registerErrors = [
        "4"  => "Les mots de passe ne correspondent pas",
        "3"  => "Nom requis",
        "2"  => "Le mot de passe doit faire 6 caractères ou plus",
        "1"  => "Mail invalide",
        "0"  => "OK",
        "-1" => "Nom déjà utilisé",
        "-2" => "Mail déjà utilisé",
        "-3" => "Erreur inconnue"
    ];

    $connectErrors = [
        "2"  => "Mot de passe requis",
        "1"  => "Adresse mail requise",
        "0"  => "OK",
        "-1" => "Identifiants invalide"
    ];

    $ticketErrors = [
        "4"  => "Merci de spécifier l'emplacement",
        "3"  => "Merci de spécifier une priorité",
        "2"  => "Description obligatoire",
        "1"  => "Sujet obligatoire",
        "0"  => "OK",
        "-1" => "Error inconnue",
        "-2" => "Vous devez vous reconnecter"
    ];


    // var_dump($_SESSION);

    if(!$Auth->connected){


        if(isset($_REQUEST['singup'])){ ?>

            <div id="form-container">
                <form method="post" action="/misc/register.php?goto=formTicket?singup" id="ticket-form" class="singup">
        
                    <div id="form-title">
                        Inscription
                    </div>
        
                    <div class="form-group">
                        <label for="inputName">Nom</label>
                        <input type="text" class="form-control" id="inputName" aria-describedby="emailHelp" placeholder="Votre nom" name="name">
                    </div>

                    <div class="form-group">
                        <label for="inputEmail">Addresse Mail</label>
                        <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Votre e-mail" name="mail">
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Mot de passe</label>
                        <input type="password" class="form-control" id="inputPassword" placeholder="Mot de passe" name="pass">
                    </div>
                    <div class="form-group">
                        <label for="inputPasswordConfirm">Confirmation mot de passe</label>
                        <input type="password" class="form-control" id="inputPasswordConfirm" placeholder="Confirmer" name="re_pass">
                    </div>

                    <div class="submit-btn-container">
                        <button type="submit" class="btn btn-primary">S'inscrire</button>
                        <a id="singup" href="/formTicket" class="form-text text-muted">Se connecter</a>            
                    </div>
        
                    <?php if(isset($_REQUEST['errorCode']) && $_REQUEST['errorCode']!="0"): ?>

                    <div id="error-msg" class="form-text"><?=$registerErrors[$_REQUEST['errorCode']]?></div>            
                    
                    <?php endif; ?>
                </form>
            </div>

        <?php }else{  ?>

            <div id="form-container">
                <form method="post" action="/misc/connect.php?goto=formTicket" id="ticket-form">

                    <div id="form-title">
                        Connexion
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Addresse Mail</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="mail">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mot de passe</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass">
                    </div>
                    <div class="submit-btn-container">
                        <button type="submit" class="btn btn-primary">Se connecter</button>
                        <a id="singup" href="/formTicket?singup=" class="form-text text-muted">S'inscrire</a>            
                    </div>

                    <?php if(isset($_REQUEST['errorCode']) && $_REQUEST['errorCode']!="0"): ?>

                    <div id="error-msg" class="form-text"><?=$connectErrors[$_REQUEST['errorCode']]?></div>            

                    <?php endif; ?>

                </form>
            </div>
    
        <?php } ?>

    <?php }else{ ?>

            <div id="form-container">
                <form method="post" action="/misc/ticket.php?goto=formTicket" id="ticket-form" class="ticket">

                    <div id="form-title">
                        Poster un ticket
                    </div>

                    <div class="form-group">
                        <label for="subject-input">Sujet</label>
                        <input type="text" class="form-control" id="subject-input" aria-describedby="emailHelp" placeholder="Sujet de l'incident" name="subject">
                    </div>
                    <div class="form-group">
                        <label for="desc-input">Description</label>
                        <textarea type="text" rows="5" class="form-control" id="desc-input" placeholder="Décrivrez l'incident" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="prio-select">Priorité</label>
                        <select name="prio" id="prio-select" class="form-control">
                            <option value="0">--Choisissez une priorité--</option>
                            <option value="1">Très faible</option>
                            <option value="2">Faible</option>
                            <option value="3">Normale</option>
                            <option value="4">Élevée</option>
                            <option value="5">Très élevée</option>
                            <option value="6">Critique</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label for="location-input">Emplacement</label>
                        <input type="text" class="form-control" id="location-input" placeholder="Où se déroule-t-il ?" name="location">
                    </div>
                    <div class="submit-btn-container">
                        <button type="submit" class="btn btn-primary">Envoyer</button>
                        <a id="see-my-tickets" href="/mytickets" class="form-text text-muted">Voir mes tickets</a>            
                    </div>

                    <?php if(isset($_REQUEST['ticketCode']) && $_REQUEST['ticketCode']!="0"): ?>

                    <div id="error-msg" class="form-text"><?=$ticketErrors[$_REQUEST['ticketCode']]?></div>            

                    <?php endif; ?>

                    <?php if(isset($_REQUEST['ticketCode']) && $_REQUEST['ticketCode']=="0"): ?>

                    <div id="success-msg" class="form-text">Ticket envoyé</div>            

                    <?php endif; ?>

                </form>
            </div>


    <?php } ?>


    <?=$Navbar->script?>

</body>


</html>